# Changelog

## 0.0.2
* Fixed Android issue where the bottom of the screenshot would slice off part of the app view in certain configurations.

## 0.0.1
* Initial release
