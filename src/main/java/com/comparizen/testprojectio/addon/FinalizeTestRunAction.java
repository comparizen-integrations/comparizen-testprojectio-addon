package com.comparizen.testprojectio.addon;

import com.comparizen.client.ComparizenClient;
import com.comparizen.client.ComparizenClientException;
import com.comparizen.client.model.TestRunStatus;
import com.comparizen.client.model.TestRunStatusResponse;
import io.testproject.java.annotations.v2.Action;
import io.testproject.java.annotations.v2.Parameter;
import io.testproject.java.enums.ParameterDirection;
import io.testproject.java.sdk.v2.addons.GenericAction;
import io.testproject.java.sdk.v2.addons.helpers.AddonHelper;
import io.testproject.java.sdk.v2.enums.ExecutionResult;
import io.testproject.java.sdk.v2.exceptions.FailureException;

@Action(name = "Finalize Comparizen test-run")
public class FinalizeTestRunAction implements GenericAction {

	@Parameter(description = "API key")
	public String apiKey;

	@Parameter(description = "Test-run ID")
	public String testRunId;

	@Parameter(description = "Comparizen report url", direction = ParameterDirection.OUTPUT)
	public String reportUrl;

	@Override
	public ExecutionResult execute(AddonHelper helper) throws FailureException {
		ComparizenClient client = ComparizenClient.builder()
			.apiKey(apiKey)
			.build();

		try {
			client.finalizeTestRun(testRunId);
			TestRunStatusResponse result = client.waitUntilTestRunResult(testRunId, 30000);
			helper.getReporter().result("got result " + result.toString());
			this.reportUrl = result.getUrl();
			if (result.getTestRunStatus() == TestRunStatus.ALL_OK) {
				return ExecutionResult.PASSED;
			} else {
				helper.getReporter().result("Diffs were found in uploaded screenshots. See Comparizen report for more details.");
				return ExecutionResult.FAILED;
			}
		} catch (ComparizenClientException | InterruptedException e) {
			throw new FailureException("Could not finalize Comparizen test-run: " + e.getMessage(), e);
		}
	}
}
