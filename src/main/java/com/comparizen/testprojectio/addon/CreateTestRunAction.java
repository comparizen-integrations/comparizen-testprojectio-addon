package com.comparizen.testprojectio.addon;

import com.comparizen.client.ComparizenClient;
import com.comparizen.client.ComparizenClientException;
import io.testproject.java.annotations.v2.Action;
import io.testproject.java.annotations.v2.Parameter;
import io.testproject.java.enums.ParameterDirection;
import io.testproject.java.sdk.v2.addons.GenericAction;
import io.testproject.java.sdk.v2.addons.helpers.AddonHelper;
import io.testproject.java.sdk.v2.enums.ExecutionResult;
import io.testproject.java.sdk.v2.exceptions.FailureException;

@Action(name = "Create Comparizen test-run")
public class CreateTestRunAction implements GenericAction {

	@Parameter(description = "API key")
	public String apiKey;

	@Parameter(description = "Project ID")
	public String projectId;

	@Parameter(description = "Test-run name", defaultValue = "")
	public String testRunName;

	@Parameter(description = "Generated test-run ID", direction = ParameterDirection.OUTPUT)
	public String testRunId;

	@Override
	public ExecutionResult execute(AddonHelper helper) throws FailureException {
		ComparizenClient client = ComparizenClient.builder()
			.apiKey(apiKey)
			.build();

		System.out.println("used " + apiKey);

		try {
			this.testRunId = client.createTestRun(projectId, testRunName);
		} catch (ComparizenClientException e) {
			throw new FailureException("Could not create Comparizen test-run: " + e.getMessage(), e);
		}

		return ExecutionResult.PASSED;
	}
}
