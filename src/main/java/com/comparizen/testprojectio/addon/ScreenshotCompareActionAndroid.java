package com.comparizen.testprojectio.addon;

import io.testproject.java.annotations.v2.Action;
import io.testproject.java.sdk.v2.addons.AndroidAction;
import io.testproject.java.sdk.v2.addons.helpers.AndroidAddonHelper;
import io.testproject.java.sdk.v2.enums.ExecutionResult;
import io.testproject.java.sdk.v2.exceptions.FailureException;
import org.openqa.selenium.OutputType;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;

import static com.comparizen.testprojectio.addon.AbstractScreenshotCompareAction.ACTION_DESCRIPTION;
import static com.comparizen.testprojectio.addon.AbstractScreenshotCompareAction.ACTION_NAME;

@Action(name = ACTION_NAME, description = ACTION_DESCRIPTION)
public class ScreenshotCompareActionAndroid extends AbstractScreenshotCompareAction implements AndroidAction {

	@Override
	public ExecutionResult execute(AndroidAddonHelper helper) throws FailureException {
		File fullScreenshot = helper.getDriver().getScreenshotAs(OutputType.FILE);
		Map<String, Map<String, Object>> systemBars = helper.getDriver().getSystemBars();
		if (systemBars.containsKey("statusBar")) {
			long statusBarHeight = (long) systemBars.get("statusBar").get("height");
			BufferedImage croppedImage = getCroppedImage(fullScreenshot, Integer.parseInt(String.valueOf(statusBarHeight)));
			ExecutionResult result = performScreenshotComparison(croppedImage);
			croppedImage.flush();
			return result;
		} else {
			return performScreenshotComparison(fullScreenshot);
		}
	}

	private BufferedImage getCroppedImage(File imageFile, int statusBarHeight) throws FailureException {
		try {
			BufferedImage image = ImageIO.read(imageFile);
			return image.getSubimage(0, statusBarHeight, image.getWidth(), image.getHeight() - statusBarHeight);
		} catch (IOException e) {
			throw new FailureException("Could not read screenshot image " + e.getMessage(), e);
		}
	}
}
