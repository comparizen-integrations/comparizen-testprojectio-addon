package com.comparizen.testprojectio.addon;

import io.testproject.java.annotations.v2.Action;
import io.testproject.java.sdk.v2.addons.WebAction;
import io.testproject.java.sdk.v2.addons.helpers.WebAddonHelper;
import io.testproject.java.sdk.v2.enums.ExecutionResult;
import io.testproject.java.sdk.v2.exceptions.FailureException;
import org.openqa.selenium.OutputType;

import java.io.File;

import static com.comparizen.testprojectio.addon.AbstractScreenshotCompareAction.ACTION_DESCRIPTION;
import static com.comparizen.testprojectio.addon.AbstractScreenshotCompareAction.ACTION_NAME;

@Action(name = ACTION_NAME, description = ACTION_DESCRIPTION)
public class ScreenshotCompareActionWeb extends AbstractScreenshotCompareAction implements WebAction {

	@Override
	public ExecutionResult execute(WebAddonHelper helper) throws FailureException {
		File screenshotFile = helper.getDriver().getScreenshotAs(OutputType.FILE);
		return performScreenshotComparison(screenshotFile);
	}
}
