package com.comparizen.testprojectio.addon;

import com.comparizen.client.ComparizenClient;
import com.comparizen.client.ComparizenClientException;
import com.comparizen.client.util.ImageType;
import io.testproject.java.annotations.v2.Parameter;
import io.testproject.java.sdk.v2.enums.ExecutionResult;
import io.testproject.java.sdk.v2.exceptions.FailureException;
import io.testproject.java.sdk.v2.reporters.ActionReporter;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.awt.image.BufferedImage;
import java.io.File;

public abstract class AbstractScreenshotCompareAction {

	public static final String ACTION_NAME = "Take screenshot and compare with baseline using Comparizen";
	public static final String ACTION_DESCRIPTION = "Take screenshot and compare with baseline using Comparizen";

	@Parameter(description = "API key")
	public String apiKey;

	@Parameter(description = "Test-run ID")
	public String testRunId;

	@Parameter(description = "Screenshot name")
	public String name;

	protected ExecutionResult performScreenshotComparison(File screenshotFile) throws FailureException {
		try {
			getComparizenClient().createComparison(testRunId, name, screenshotFile.toPath());
		} catch (ComparizenClientException e) {
			throw new FailureException("An error occurred while uploading screenshot to Comparizen: " + e.getMessage(), e);
		}

		return ExecutionResult.PASSED;
	}

	protected ExecutionResult performScreenshotComparison(BufferedImage imageBuffer) throws FailureException {
		try {
			getComparizenClient().createComparison(testRunId, name, imageBuffer, ImageType.PNG);
		} catch (ComparizenClientException e) {
			throw new FailureException("An error occurred while uploading screenshot to Comparizen: " + e.getMessage(), e);
		}

		return ExecutionResult.PASSED;
	}

	protected File getMobileViewPortScreenshot(final RemoteWebDriver driver, ActionReporter reporter) {
		Object result = driver.executeScript("mobile:viewportScreenshot");
		if (result instanceof String) {
			return OutputType.FILE.convertFromBase64Png((String) result);
		} else if (result instanceof byte[]) {
			return OutputType.FILE.convertFromBase64Png(new String((byte[])((byte[])result)));
		} else {
			throw new RuntimeException(String.format("Unexpected result for %s command: %s", "screenshot", result == null ? "null" : result.getClass().getName() + " instance"));
		}
	}

	private ComparizenClient getComparizenClient() {
		return ComparizenClient.builder()
			.apiKey(apiKey)
			.build();
	}
}
