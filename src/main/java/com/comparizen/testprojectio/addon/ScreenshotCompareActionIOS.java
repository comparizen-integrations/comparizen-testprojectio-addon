package com.comparizen.testprojectio.addon;

import io.testproject.java.annotations.v2.Action;
import io.testproject.java.sdk.v2.addons.IOSAction;
import io.testproject.java.sdk.v2.addons.helpers.IOSAddonHelper;
import io.testproject.java.sdk.v2.enums.ExecutionResult;
import io.testproject.java.sdk.v2.exceptions.FailureException;

import static com.comparizen.testprojectio.addon.AbstractScreenshotCompareAction.ACTION_DESCRIPTION;
import static com.comparizen.testprojectio.addon.AbstractScreenshotCompareAction.ACTION_NAME;

@Action(name = ACTION_NAME, description = ACTION_DESCRIPTION)
public class ScreenshotCompareActionIOS extends AbstractScreenshotCompareAction implements IOSAction {

	@Override
	public ExecutionResult execute(IOSAddonHelper helper) throws FailureException {
		return performScreenshotComparison(getMobileViewPortScreenshot(helper.getDriver(), helper.getReporter()));
	}
}
