# Comparizen Testprojectio Addon

Provides an Addon for [TestProject](https://testproject.io) which offers the following integrations with [Comparizen](https://comparizen.com):
* Creating new Comparizen test-runs
* Take a screenshot and upload to a test-run
* Finalize the Comparizen test-run and get the results

This Addon is available as a Community Addon in TestProject.

